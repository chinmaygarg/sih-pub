package apitest

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class CreateUserSimulation extends Simulation {

    private val baseUrl = "https://www.beta.sih.aicte-india.org"

    private val httpProtocol = http
            .baseUrl(baseUrl)
            .acceptHeader("application/json, text/javascript, */*; q=0.01")
            .acceptEncodingHeader("gzip, deflate")
            .acceptLanguageHeader("en-US,en;q=0.9")
            .contentTypeHeader("multipart/form-data; boundary=----WebKitFormBoundaryKsgellbAuI21Kgvo")
            .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36")

    private val headers_0 = Map(
        "Origin" -> "https://www.beta.sih.aicte-india.org",
        "X-CSRF-TOKEN" -> "${csrf_token}",
        "X-Requested-With" -> "XMLHttpRequest")

    private val feeder = csv("test.csv")

    private val request = http("get new team registration page")
            .get("/studentRegistration")
            .check(css("meta[name='csrf-token']", "content").saveAs("csrf_token"))

    private val request2 = http("create new user")
            .post("/studentRegistration")
            .headers(headers_0)
            .body(ElFileBody("simulation.txt"))

    private val scn = scenario("student registration")
            .exec(request)
            .feed(feeder)
            .exec(request2)

    setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)
}
