package apitest

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class SignInSimulation extends Simulation {

    private val baseUrl = "https://www.beta.sih.aicte-india.org"

    private val httpProtocol = http
            .baseUrl(baseUrl)
            .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36")

    private val headers_0 = Map(
        "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Accept-Encoding" -> "gzip, deflate, br",
        "Accept-Language" -> "en-US,en;q=0.9",
        "Pragma" -> "no-cache",
        "Upgrade-Insecure-Requests" -> "1")

    private val headers_1 = Map(
        "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Accept-Encoding" -> "gzip, deflate, br",
        "Accept-Language" -> "en-US,en;q=0.9",
        "Origin" -> "https://www.beta.sih.aicte-india.org",
        "Pragma" -> "no-cache",
        "Upgrade-Insecure-Requests" -> "1")

    private val headers_2 = Map(
        "Content-Type" -> "multipart/form-data; boundary=----WebKitFormBoundarygBThFM4NFH5ZL1kB",
        "Host" -> "www.beta.sih.aicte-india.org",
        "cache-control" -> "no-cache")


    private val feeder = csv("test_login.csv")

    private val getSignInRequest = http("get sign in page")
            .get("/signin")
            .headers(headers_0)
            .check(css("meta[name='csrf-token']", "content").saveAs("csrf_token"))

    private val loginRequest = http("login user")
            .post("/signin")
            .headers(headers_2)
            .formParam("_token", """${csrf_token}""")
            .formParam("email", """${email}""")
            .formParam("password", """${password}""")
            .check(status.is(200))

    private val teamDetails = http("get team details")
            .get("/teamDetail")
            .headers(headers_0)
            .check(status.is(200))
            .check(css("meta[name='csrf-token']", "content").saveAs("csrf_token_1"))

    private val allProblemsRequest = http("get all problems list")
            .get("/allProblemStatements")
            .headers(headers_0)
            .check(status.is(200))

    private val selectStatement = http("select a problem statement")
            .get("/participate/${problem_id}")
            .headers(headers_0)
            .check(status.is(200))

    private val submitIdea = http("submit idea")
            .post("/participate/{problem_id}")
            .headers(headers_2)
            .formParam("_token", """${csrf_token}""")
            .formParam("title", """${title}""")
            .formParam("Description", """${description}""")
            .formUpload("template", """${file_name}""")
            .formParam("Submit Idea", """Submit Idea""")
            .check(status.is(200))

    private val scn = scenario("user sign in flow")
            .exec(getSignInRequest)
            .feed(feeder)
            .exec(loginRequest)
            .exec(submitIdea)

    setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)
}
