package apitest

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class HomeScreenSimulation extends Simulation {

    val httpProtocol = http
            .baseUrl("https://www.beta.sih.aicte-india.org")
            .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36")

    val headers_0 = Map(
        "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Accept-Encoding" -> "gzip, deflate, br",
        "Accept-Language" -> "en-US,en;q=0.9",
        "Cache-Control" -> "no-cache",
        "Pragma" -> "no-cache",
        "Upgrade-Insecure-Requests" -> "1")

    val scn = scenario("RecordedSimulationHome1")
            .exec(http("request_0")
                    .get("/")
                    .headers(headers_0)
                    .check(status.is(200)))

    setUp(scn.inject(constantConcurrentUsers(10) during (60 seconds))).protocols(httpProtocol)
}
